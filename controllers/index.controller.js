const home = (req, res) => {
  return res.render("pages/default/home");
};
const notFound = (req, res) => {
  return res.render("pages/default/not-found");
};
const error = (req, res) => {
  return res.render("pages/default/error");
};

module.exports = {
  home,
  notFound,
  error,
};
