const { Product, Sequelize } = require("../models/");

//HOME TAMPILKAN SEMUA ITEM
const home = async (req, res) => {
  let totalBarang;
  let semuaBarang;
  try {
    totalBarang = await Product.count();
    semuaBarang = await Product.findAll();
  } catch (error) {
    return res.render("pages/default/error");
  }

  const locals = {
    data: {
      totalBarang: totalBarang,
      semuaBarang: semuaBarang,
    },
    contentTitle: "Semua Barang",
    layout: "layouts/dashboard",
  };
  return res.render("pages/dashboard/home", locals);
};

//BUAT BARU PAGE
const showCreatePage = async (req, res) => {
  const locals = {
    contentTitle: "Silahkan Masukkan Data yang akan Dibuat",
    layout: "layouts/dashboard",
  };

  res.render("pages/dashboard/create", locals);
};

//BUAT BARU
const create = async (req, res) => {
  const { no, namaBarang, stok, jumlahTerjual, tanggalTransaksi, jenisBarang } =
    req.body;
  let semuaBarang;
  try {
    await Product.create({
      id: null,
      no: no,
      namaBarang: namaBarang,
      stok: stok,
      jumlahTerjual: jumlahTerjual,
      tanggalTransaksi: tanggalTransaksi,
      jenisBarang: jenisBarang,
      createdAt: null,
      updatedAt: null,
    });
    semuaBarang = await Product.findAll();
  } catch (error) {
    return res.render("pages/default/error");
  }
  const locals = {
    data: {
      semuaBarang: semuaBarang,
    },
    contentTitle: "Data Sudah Berhasil Dibuat! Coba diperiksa",
    layout: "layouts/dashboard",
  };

  res.render("pages/dashboard/home", locals);
};

//PILIH SATU
const searchOne = async (req, res) => {
  let items;
  try {
    items = await Product.findOne({ where: { id: req.params.id } });
  } catch (error) {
    return res.render("pages/default/error");
  }

  const locals = {
    data: {
      barang: items,
    },
    contentTitle: "Edit Barang",
    layout: "layouts/dashboard",
  };

  res.render("pages/dashboard/edit", locals);
};

//UBAH
const edit = async (req, res) => {
  try {
    Product.update(req.body, { where: { id: req.params.id } });
  } catch (error) {
    return res.render("pages/default/error");
  }
  const locals = {
    data: {
      barang: req.body,
    },
    contentTitle: "Sudah Berhasil Diubah!",
    layout: "layouts/dashboard",
  };

  res.render("pages/dashboard/edit", locals);
};

//DELETE
const remove = async (req, res) => {
  let totalBarang;
  let semuaBarang;
  try {
    await Product.destroy({ where: { id: req.params.id } });
    semuaBarang = await Product.findAll();
    totalBarang = await Product.count();
  } catch (error) {
    return res.render("pages/default/error");
  }
  const locals = {
    data: {
      semuaBarang: semuaBarang,
      totalBarang: totalBarang,
    },
    contentTitle: "Data sudah dihapus!",
    layout: "layouts/dashboard",
  };

  res.render("pages/dashboard/home", locals);
};

//ERROR HANDLING
const notFound = (req, res) => {
  return res.render("pages/default/not-found");
};
const error = (req, res) => {
  return res.render("pages/default/error");
};

module.exports = {
  home,
  notFound,
  error,
  searchOne,
  edit,
  remove,
  create,
  showCreatePage,
};
