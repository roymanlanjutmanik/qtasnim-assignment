const { Op } = require("sequelize");
const { Product, sequelize } = require("../models/");

// DATA TAMPILAKAN SEMUA ITEM

const data = async (req, res) => {
  let semuaBarang;
  try {
    semuaBarang = await Product.findAll();
  } catch (error) {
    return res.render("pages/default/error");
  }

  const locals = {
    data: {
      semuaBarang: semuaBarang,
    },
    contentTitle: "Olah Data",
    layout: "layouts/dashboard",
  };
  return res.render("pages/data/data", locals);
};

const minMax = async (req, res) => {
  let penjualanSedikit;
  let penjualanBanyak;

  try {
    let min = await Product.min("jumlahTerjual");
    penjualanSedikit = await Product.findOne({ where: { jumlahTerjual: min } });

    let max = await Product.max("jumlahTerjual");
    penjualanBanyak = await Product.findOne({ where: { jumlahTerjual: max } });
  } catch (error) {
    return res.render("pages/default/error");
  }
  const locals = {
    data: {
      penjualanSedikit: penjualanSedikit,
      penjualanBanyak: penjualanBanyak,
    },
    contentTitle: "Penjualan sedikit vs Penjualan terbanyak",
    layout: "layouts/dashboard",
  };

  res.render("pages/data/compare", locals);
};

//URUT BERDASARKAN NAMA
const sortByName = async (req, res) => {
  let semuaBarang;
  try {
    semuaBarang = await Product.findAll({
      order: [["namaBarang", "ASC"]],
    });
  } catch (error) {
    return res.render("pages/default/error");
  }

  const locals = {
    data: {
      semuaBarang: semuaBarang,
    },
    contentTitle: "Data sudah diurutkan!",
    layout: "layouts/dashboard",
  };
  return res.render("pages/data/data", locals);
};

//URUT BERDASARKAN TANGGAL
const sortByDate = async (req, res) => {
  let semuaBarang;
  try {
    semuaBarang = await Product.findAll({
      order: [["tanggalTransaksi", "ASC"]],
    });
  } catch (error) {
    return res.render("pages/default/error");
  }

  const locals = {
    data: {
      semuaBarang: semuaBarang,
    },
    contentTitle: "Data sudah diurutkan!",
    layout: "layouts/dashboard",
  };
  return res.render("pages/data/data", locals);
};

// const where = {
//   [Op.or]: [{
//       from: {
//           [Op.between]: [startDate, endDate]
//       }
//   }, {
//       to: {
//           [Op.between]: [startDate, endDate]
//       }
//   }]
// };
// ANTARA TANGGAL
const betweenDate = async (req, res) => {
  let start = new Date(req.body.start);
  let end = new Date(req.body.end);
  let semuaBarang;
  try {
    semuaBarang = await Product.findAll({
      where: {
        tanggalTransaksi: {
          [Op.between]: [start, end],
        },
      },
    });
  } catch (error) {
    return res.render("pages/default/error");
  }

  const locals = {
    data: {
      semuaBarang: semuaBarang,
    },
    contentTitle: "Data sudah diurutkan!",
    layout: "layouts/dashboard",
  };
  return res.render("pages/data/data", locals);
};

//ERROR HANDLING
const notFound = (req, res) => {
  return res.render("pages/default/not-found");
};
const error = (req, res) => {
  return res.render("pages/default/error");
};

module.exports = {
  data,
  minMax,
  sortByName,
  sortByDate,
  betweenDate,
  notFound,
  error,
};
