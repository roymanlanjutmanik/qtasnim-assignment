"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Products", [
      {
        no: 1,
        namaBarang: "Kopi",
        stok: 100,
        jumlahTerjual: 10,
        tanggalTransaksi: new Date(2021, 4, 1).toLocaleDateString("id"),
        jenisBarang: "Konsumsi",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        no: 1,
        namaBarang: "Teh",
        stok: 100,
        jumlahTerjual: 19,
        tanggalTransaksi: new Date(2021, 4, 5).toLocaleDateString("id"),
        jenisBarang: "Konsumsi",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        no: 1,
        namaBarang: "Kopi",
        stok: 90,
        jumlahTerjual: 15,
        tanggalTransaksi: new Date(2021, 4, 10).toLocaleDateString("id"),
        jenisBarang: "Konsumsi",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        no: 1,
        namaBarang: "Pasta Gigi",
        stok: 100,
        jumlahTerjual: 20,
        tanggalTransaksi: new Date(2021, 4, 11).toLocaleDateString("id"),
        jenisBarang: "Pembersih",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        no: 1,
        namaBarang: "Sabun Mandi",
        stok: 100,
        jumlahTerjual: 30,
        tanggalTransaksi: new Date(2021, 4, 11).toLocaleDateString("id"),
        jenisBarang: "Pembersih",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        no: 1,
        namaBarang: "Sampo",
        stok: 100,
        jumlahTerjual: 25,
        tanggalTransaksi: new Date(2021, 4, 12).toLocaleDateString("id"),
        jenisBarang: "Pembersih",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        no: 1,
        namaBarang: "Teh",
        stok: 81,
        jumlahTerjual: 5,
        tanggalTransaksi: new Date(2021, 4, 12).toLocaleDateString("id"),
        jenisBarang: "Konsumsi",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Products", null, {});
  },
};
