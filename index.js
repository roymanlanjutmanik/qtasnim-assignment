const express = require("express");
const morgan = require("morgan");
const bodyParser = require ("body-parser")
const app = express();
const { PORT = 3000 } = process.env;
const expressLayout = require("express-ejs-layouts");
const router = require("./routes/index");


//View Engine
app.use(express.static("public"));
app.set("view engine", "ejs");
app.use(expressLayout);
app.set("layout", "layouts/default");

//morgan
app.use(morgan("dev"));

//parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

//Md content-title
const setDefault = (req, res, next) => {
  res.locals.contentTitle = "Default";
  next();
};

//router
app.use(setDefault);
app.use(router);

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
