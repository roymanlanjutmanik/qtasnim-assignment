const express = require("express");
const router = express.Router();
const dashboardCtrl = require("../controllers/dashboard.controller");

// /dasboard
router.get("/", dashboardCtrl.home);

// dashboard/create
router.get("/create", dashboardCtrl.showCreatePage);
router.post("/create", dashboardCtrl.create);

// dashboard/edit;
router.get("/edit/:id", dashboardCtrl.searchOne);
router.post("/edit/:id", dashboardCtrl.edit);

// dashboard/delete;
router.get("/delete/:id", dashboardCtrl.remove);

module.exports = router;
