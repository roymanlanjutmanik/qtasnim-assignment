const express = require("express");
const router = express.Router();
const dashboardRoute = require("./dashboard.router");
const dataRoute = require("./data.router");
const indexCtrl = require("../controllers/index.controller");


router.get("/", indexCtrl.home);

router.use("/dashboard", dashboardRoute);
router.use("/data", dataRoute);

router.use(indexCtrl.notFound);
router.use(indexCtrl.error);

module.exports = router;
