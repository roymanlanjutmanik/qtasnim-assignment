const express = require("express");
const router = express.Router();
const dataCtrl = require("../controllers/data.controller");


router.get("/", dataCtrl.data);

//Penjualan sedikit vs Penjualan terbanyak
router.get("/compare", dataCtrl.minMax);

//Urutkan data by name / date
router.get("/sortname", dataCtrl.sortByName);
router.get("/sortdate", dataCtrl.sortByDate);

//Sorting berdasarkan tanggal
router.post("/betweendate", dataCtrl.betweenDate);

module.exports = router;
