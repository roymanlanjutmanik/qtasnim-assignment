"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Products", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      no: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      namaBarang: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      stok: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      jumlahTerjual: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      tanggalTransaksi: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      jenisBarang: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Products");
  },
};
